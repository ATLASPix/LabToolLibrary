# Tools for control of instrument in 161 lab (UNIGE)

## Dependencies

Based on:

* https://pyvisa.readthedocs.io/en/stable/ Install following instruction
* https://github.com/applied-optics/vxi11.git Install following instruction
* https://github.com/baldwint/wanglib Download from git and execute, from the wanglib folder `sudo python setup.py install`


## Notes for using GPIB-USB with prologix controller

Set permission for using serial channel by running : 

`sudoedit /etc/udev/rules.d/50-ttyusb.rules`

and stick this in there:

`KERNEL=="ttyUSB[0-9]*",NAME="tts/USB%n",SYMLINK+="%k",GROUP="uucp",MODE="0666"`


## Environnement
To setup your PYTHONPATH to use all the librairies source setup.sh from the ToolControls folder
