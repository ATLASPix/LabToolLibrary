from wanglib import prologix

class interface:

    plx=0
    inst=0

    def __init__(self,mode='USB',address='/dev/ttyUSB1',gpib_addr=24):

        if mode=='USB':
            self.plx = prologix.prologix_USB(address)
            self.inst=prologix.instrument(self.plx,24,auto=False,delay=0.05)
            self.plx.auto=False
        elif mode=='ETHERNET':
            self.plx = prologix.prologix_ethernet(address)
            self.inst=prologix.instrument(self.plx,24)

    def write(self,cmd):
        self.inst.write(cmd)
        return cmd

    def query(self,cmd):
        return self.inst.ask(cmd)