import visa
import numpy as np
import time
from threading import Thread
from ROOT import *
from math import *
from PrologixGPIB import *


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step



class k2410:
    """Keithley Control"""

    rm=0
    inst=0
    hold=1

    targetVoltage=0

    def __init__(self):
        # self.rm = visa.ResourceManager('@py')
        # self.rm.list_resources()
        # self.inst = self.rm.open_resource("GPIB0::24::INSTR'")

        self.inst=interface()
        print self.inst.write(":CONFIGURE:CURR")
        print self.inst.write(":SOURCE:VOLT:MODE FIX")
        print self.inst.write(":SOURCE:VOLT:RANG MIN")
        print self.inst.write(":SOURCE:VOLT:RANG MIN")
        print self.inst.write(":FORM ASCii")
        print self.inst.write(":FORM:ELEM VOLT, CURR")


    def ID(self):
        print(self.inst.query("*IDN?"))

    def setIRange(self,value):
        self.inst.write(":SENS:CURR:RANG %f"%value)

    #
    def setVRange(self,maxV):
        self.inst.write(":SOURCE:VOLT:RANG %f"%maxV)


    def setOutput(self,state):
        self.inst.write(":OUTPUT:STATE %i"%state)



    def setVoltage(self,value):
        self.inst.write(":SOURCE:VOLT:IMM:AMPL %f"%value)

    def setCurrentLimit(self, value):
        self.inst.write(":SENS:CURR:PROT:LEV %f"%value)

    def getCurrent(self):
        return self.inst.query(":MEAS?")

    def setTargetVoltage(self,voltage):
        self.targetVoltage=voltage

    def rampUp(self,step=1):
        self.setOutput(1)
        if self.targetVoltage<0:
            ssign=-1
        else:
            ssign=1

        for v in np.arange(0,self.targetVoltage+ssign*step,ssign*step):
            time.sleep(0.01)
            self.setVoltage(v)
            meas = keith.getCurrent()
            print meas

    def rampDown(self,step=1):
        #print np.arange(0,voltage,(voltage/abs(voltage))*0.1)
        self.hold=0
        ssign=0
        if self.targetVoltage<0:
            ssign=1
        else:
            ssign=-1

        for v in np.arange(self.targetVoltage,ssign*step,ssign*step):
            #time.sleep(0.01)
            self.setVoltage(v)
            meas = keith.getCurrent()
            print meas

        self.setOutput(0)

    def MaintainVoltage(self,value):

        keith.setTargetVoltage(value)
        keith.rampUp()
        self.hold=1
        #gr=TGraph()
        #Th=Thread(target=self.CurrentReadLoop(gr))
        #Th.start()
        #print "blah"

    def CurrentReadLoop(self,gr):
        i=0
        can=TCanvas()
        can.Draw()
        gr.Draw("a*")
        while(self.hold):
            gr.SetPoint(i,time.clock(),self.getCurrent()[1])
            can.Update()
            i+=1
            time.sleep(0.5)
            #print self.getCurrent()


if __name__ == '__main__':
    keith = k2410()
    print keith.ID()
    keith.setVRange(200)
    keith.setIRange(100e-6)
    #keith.setVoltage(-10)
    keith.setCurrentLimit(100e-6)
    #keith.setOutput(1)
    keith.MaintainVoltage(-45)

    while(1):
        a=raw_input("k2410>")
        if a=="off":
            keith.rampDown()
        if a=="cur":
            print keith.getCurrent()
        if a=="on":
            print keith.rampUp()

