import visa

class TG5011:
    """TG5011 Control"""

    rm=0
    inst=0

    def __init__(self):
        self.rm = visa.ResourceManager('@py')
        #self.rm.list_resources()
        self.inst = self.rm.open_resource("TCPIP0::192.168.1.200::9221::SOCKET")
        self.inst.read_termination="\n"


    def ID(self):
        print self.inst.query("*IDN?")

    def setPulse(self,amplitude,frequency,width,count):
        print self.inst.write("WAVE PULSE")
        print self.inst.write("AMPUNIT VPP")
        print self.inst.write("AMPL %f"%amplitude)
        #
        self.inst.write("PULSFREQ %f"%frequency)
        self.inst.write("PULSWID %f"%width)
        self.inst.write("BST NCYC")
        self.inst.write("BSTCOUNT %i"%count)
        self.inst.write("OUTPUT ON")
        self.inst.write("BSTTRGSRC MAN")

    def trigger(self):
        self.inst.write("*TRG")

pulser=TG5011()

if __name__ == '__main__':
    pulser.ID()
    pulser.setPulse(1.8, 1e4, 1e-6, 100)

    while(1):
        a=raw_input("TG5011>")
        if a=="pulse":
            pulser.trigger()