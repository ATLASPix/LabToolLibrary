import vxi11
import numpy as np
import time
import visa
import ROOT



class HD06104:
    """TG5011 Control"""

    rm=0
    inst=0

    def __init__(self,ip):
        self.inst=vxi11.Instrument("%s"%ip)
        self.inst.write("WAVEFORM_SETUP SP,0,NP,0,FP,0,SN,0")
        self.inst.write("COMM_HEADER OFF")
    def ID(self):
        print self.inst.ask("*IDN?")


    def setTimeDivision(self,base_div):
        self.inst.write("TIME_DIV %f"%base_div)
        print self.inst.ask("TIME_DIV?")


    def setTriggerMode(self,mode): # AUTO,SINGLE,NORM,STOP
        self.inst.write("TRIG_MODE %s"%mode)

    def setTrigger(self,channel,edge,level):
        self.inst.write("%s:TRCP DC"%channel)
        self.inst.write("%s:TRIG_DELAY 0"%channel)
        self.inst.write("%s:TRIG_LEVEL %fV"%(channel,level))
        self.inst.write("TRIG_SELECT EDGE,SR,%s"%channel)
        self.inst.write("%s:TRIG_SLOPE %s"%(channel,edge))

    def setVerticalScale(self,channel,scale):
        self.inst.write("%s:VOLT_DIV %fV"%(channel,scale))

    def getSingleWF(self,channel):
        #self.inst.write("STO C2,M1")
        #self.inst.write("%s:WAVEFORM? ALL"%"M1")
        r=self.inst.ask("C2:INSPECT? DATA_ARRAY_1, FLOAT")
        rs=r.split()

        dt=float(self.inst.ask("C2:INSPECT? HORIZ_INTERVAL").split()[2])

        data=[float(x) for x in rs[1:len(rs)-1]]
        #print data

        gr=ROOT.TGraph(len(data))
        [gr.SetPoint(i,i,point) for i,point in enumerate(data)]
        return gr


    def trigger(self):
        self.inst.write("*TRG")

if __name__ == '__main__':
    osc=HD06104("192.168.1.7")
    osc.ID()
    osc.setTimeDivision(2e-6)
    osc.setTrigger("C2","NEG",0.1)
    osc.setTriggerMode("NORMAL")

    osc.setVerticalScale("C1",1)
    osc.setVerticalScale("C2", 0.3)
    osc.setVerticalScale("C3", 0.3)
    osc.setVerticalScale("C4",0.2)
    osc.trigger()

    gr=osc.getSingleWF("C2")
    gr.Draw("AL")
    a=raw_input()